import React = require("react");
import ReactDOM = require("react-dom");
import YearForm from './components/YearForm'
import Article from './components/Article'
import Map from './components/Map'

class AppState {
  choosedYear: {};
  years: Array<{}>;
}

export class App extends React.Component<{}, AppState> {
  constructor(props: any) {
    super(props);
    this.state = {
      choosedYear: {},
      years: [{ id: 0, year: '1963' }, { id: 1, year: '1996' }, { id: 2, year: '2001' }]
    }
  }
  chooseYear = (year: string, id: number) => {
    console.log("choose year")
    console.log(year)
    this.setState({
      choosedYear: { id: id, year: year }
    });
  }
  changeYear = (year: string, id: number) => {
    let years: Array<{}> = this.state.years;
    let choosed: {} = years.find((value: {}) => { return value['id'] == id });
    years[years.indexOf(choosed)] = { id: id, year: year };
    this.setState({
      years: years,
      choosedYear: {}
    });
  }


  render() {
    return (

      <div className={"box1"}>
        <YearForm changeYear={this.changeYear} choosedYear={this.state.choosedYear} />
        <h4></h4>
        <svg height={"100%"} width={"100%"}>
          <Map />
          {
            this.state.years.map((yearId: {}) =>
              <Article yearId={yearId} key={yearId['id']} chooseYear={this.chooseYear} x={2} y={2 + yearId['id'] * 100} />
            )
          }
        </svg >
      </div >

    );
  }
}


ReactDOM.render(
  (<App />),
  document.getElementById('root')
);

declare let module: any
if (module.hot) {
  module.hot.accept();
}
