import React = require("react");
import { drawArticle } from './ArticleD3'

class ArticleProps {
  yearId: {};
  chooseYear: (year: string, id: number) => void;
  x: number;
  y: number;
}

class ArticleState {
  yearId: {};
  svgRef: any;
}

export class Article extends React.Component<ArticleProps, ArticleState> {
  constructor(props: ArticleProps) {
    super(props)
    this.state = {
      yearId: this.props.yearId,
      svgRef: React.createRef()
    };
  }

  componentDidMount() {
    console.log("ArticleDidMount")
    console.log(this.state.yearId['id'])
    drawArticle(this.state.svgRef.current, [this.state.yearId], this.props.chooseYear);
  }

  componentWillReceiveProps(nextProps: ArticleProps) {
    console.log("ArticleWillReceiveProps")
    if (nextProps.yearId !== this.props.yearId) {
      this.setState({
        yearId: nextProps.yearId
      });
    }
  }

  componentDidUpdate(prevProps: ArticleProps) {
    console.log("ArticleDidUpdate")
    if (prevProps.yearId !== this.props.yearId) {
      drawArticle(this.state.svgRef.current, [this.state.yearId], this.props.chooseYear);
    }
  }

  componentWillUnmount() {
    console.log("ArticleWillUnmount")
    drawArticle(this.state.svgRef.current, [this.state.yearId], this.props.chooseYear)
  }

  render() {
    return (
      <g ref={this.state.svgRef}>
        <rect x={this.props.x} y={this.props.y} width={96} height={46} fill={"white"} stroke={"orange"} strokeWidth={2} >
        </rect>
        <text x={5 + this.props.x} y={20 + this.props.y}>
        </text>
      </g>
    );
  }
}
export default Article;
