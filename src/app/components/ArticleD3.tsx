import * as d3 from "d3";

export function drawArticle(selection: any, data: Array<{}>, chooseYear: (year: string, id: number) => void) {
  console.log("draw article")
  console.log(data[0]['id']);
  const rect = d3.select(selection)
  rect
    .data(data)
    .on("click", (d) => chooseYear(d['year'], d['id']))

  rect.selectAll("text")
    .data(data)
    .enter()
    .text((d) => d['year'])


  rect.selectAll("text")
    .data(data)
    .transition()
    .duration(100)
    .text((d) => d['year'])


  rect.selectAll("text")
    .data(data)
    .exit()
    .remove()

}
/*

export function drawArticle(selection: any, data: Array<{}>, chooseYear: (year: string, id: number) => void) {
  console.log("draw article")
  console.log(data['id']);
  const svg = d3.select(selection).append("svg")
    .attr("width", 100)
    .attr("height", 100)
    .style("margin-left", 100);
  svg.selectAll("rect")
    .data(data)
    .enter()
    .append("rect")
    .attr("x", 27)
    .attr("y", 100 - 52)
    .attr("width", 50)
    .attr("height", 50)
    .attr("fill", "white")
    .attr("stroke", "orange")
    .attr("stroke-width", "2")
    .on("click", () => chooseYear(data['year'], data['id']))

  svg.selectAll("text")
    .data(data)
    .enter()
    .append("text")
    .text((d) => d['year'])
    .attr("x", 28)
    .attr("y", 76)

}

export function updateArticle(selection: any, data: Array<{}>) {
  console.log("update article")
  console.log(data["id"]);
  const svg = d3.select(selection).select("svg");
  svg.selectAll("text")
    .data(data)
    .transition()
    .duration(100)
    .text((d) => d['year'])

}

export function removeArticle(selection: any, data: Array<{}>) {
  console.log("remove article")
  console.log(data["id"]);
  const svg = d3.select(selection);
  svg.data(data).remove()

}
*/
