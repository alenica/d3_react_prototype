import React = require("react");
import { drawMap } from './MapD3'
import { rgb } from "d3-color";

class MapState {
  gRef: any;
  translate: Array<number>;
  scale: number;
}
export class Map extends React.Component<{}, MapState> {
  constructor(props: any) {
    super(props)
    this.state = {
      gRef: React.createRef(),
      translate: [0, 0],
      scale: 1.0
    };
  }
  componentDidMount() {
    console.log("MapDidMount")
    drawMap(this.state.gRef.current, this.transFunc);
  }

  componentDidUpdate(prevProps: any) {
    console.log("MapDidUpdate")

  }

  transFunc = (translate: any, scale: any) => {
    /*  this.setState({
        translate: translate,
        scale: scale
      });*/
  };

  render() {
    return (
      <g id={"map"} ref={this.state.gRef}>

      </g>
    );
  }
}

export default Map;


/*
<circle r="250" cx="600" cy="260" stroke={rgb(220, 220, 220).toString()} fill={'white'}></circle>
<circle r="100" cx="600" cy="260" stroke={rgb(220, 220, 220).toString()} fill={'white'}></circle>
*/
