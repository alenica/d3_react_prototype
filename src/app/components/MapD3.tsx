import * as d3 from "d3";
import { rgb } from "d3";

export function drawMap(selection: any, transFunc: (translate: any, scale: any) => void) {

  /*var zoom = d3.zoom()
    .scaleExtent([1, 10])
    .on('zoom', zoomFn);

  function zoomFn() {
    console.log(d3.event.transform.x)
    transFunc([d3.event.transform.x, d3.event.transform.y], d3.event.transform.k);
    //d3.select(selection).attr('transform', 'translate(' + d3.event.transform.x + ',' + d3.event.transform.y + ') scale(' + d3.event.transform.k + ')');
  }
  d3.select(selection).call(zoom);*/
  //selection = "g#map"
    d3.select(selection)
      .append("circle")
      .attr("r", 250)
      .attr("cx", 600)
      .attr("cy", 260)
      .attr("stroke", rgb(220, 220, 220).toString())
      .attr("fill", 'white')
    d3.select(selection)
      .append("circle")
      .attr("r", 100)
      .attr("cx", 600)
      .attr("cy", 260)
      .attr("stroke", rgb(220, 220, 220).toString())
      .attr("fill", 'white')

    function zoomed() {
      // console.log("zooming");
      // console.log(d3.event.transform.x)
      // console.log(d3.event.transform.y)
      // console.log(d3.event.transform.k)
      //transFunc([d3.event.transform.x, d3.event.transform.y], d3.event.transform.k);
      d3.select(selection).attr("transform", 'translate(' + d3.event.transform.x + ',' + d3.event.transform.y + ') scale(' + d3.event.transform.k + ')');
    }

    d3.select(selection)
      .call(d3.zoom()
        .scaleExtent([1, 10])
        .on("zoom", zoomed))
}
