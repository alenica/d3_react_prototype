import FormGroup = require("react-bootstrap/lib/FormGroup");
import ControlLabel = require("react-bootstrap/lib/ControlLabel");
import FormControl = require("react-bootstrap/lib/FormControl");
import React = require("react");
import Button = require("react-bootstrap/lib/Button");

class YearFormProps {
  choosedYear: {};
  changeYear: (year: string, id: number) => void;
}

class YearFormState {
  year: string;
}

export class YearForm extends React.Component<YearFormProps, YearFormState> {
  constructor(props: YearFormProps) {
    super(props);
    this.state = {
      year: this.props.choosedYear['year']
    }
  }
  componentWillReceiveProps(nextProps: YearFormProps) {
    console.log("YearFormWillReceiveProps")
    if (nextProps !== this.props) {
      this.setState({
        year: nextProps.choosedYear['year']
      });
    }
  }
  handleOnChange = (e: any) => {
    this.setState({
      year: e.target.value
    });
  }
  handleOnClickChange = () => {
    this.props.changeYear(this.state.year, this.props.choosedYear['id'])
    this.setState({
      year: ''
    });
  }
  render() {
    return (
      <form>
        <FormGroup controlId="formBasicText">
          <ControlLabel>Enter the year</ControlLabel>
          <h4></h4>
          <FormControl
            type="text"
            value={this.state.year}
            placeholder="Year"
            onChange={this.handleOnChange}
          />
          <FormControl.Feedback />
          <h4></h4>
          <Button bsStyle="primary" onClick={this.handleOnClickChange}>Change</Button>
        </FormGroup>
      </form>
    );
  }
}

export default YearForm;
